let carto = document.querySelector("#carto");

carto.addEventListener("mousemove", updateTitle);

function updateTitle(e) {
  var pos = [];
  pos['x'] 	= Math.floor((e.pageX - carto.offsetLeft) / 3);
  pos['y'] 	= Math.ceil((600 - (e.pageY - carto.offsetTop)) / 3);
  pos['xy'] 	= pos['x'] +','+ pos['y'];
  document.title = pos['xy'];
}