chrome.storage.local.get("nostalgic", function(items) {
  if(!items.nostalgic) { return; }

  let originalCodeSource = document.documentElement.innerHTML;

  function injectStyles(url) {
    var elem = document.createElement('link');
    elem.rel = 'stylesheet';
    elem.setAttribute('href', url);
    document.body.appendChild(elem);
  }

  injectStyles(chrome.extension.getURL('styles.css'));

  function injectFavicon(url) {
    var elem = document.createElement('link');
    elem.rel = 'icon';
    elem.setAttribute('href', url);
    document.head.appendChild(elem);
  }

  injectFavicon(chrome.extension.getURL('favicon.png'));


  let orig = document.createElement('a');
  orig.style="font-size:8px;color:white;cursor:pointer;";
  orig.innerText = "Afficher la page originale";
  orig.onclick = function() { 
    document.documentElement.innerHTML = originalCodeSource; 
  };
  document.body.appendChild(orig);

});