chrome.storage.local.get("nostalgic", function(items) {
  if(!items.nostalgic) { return; }
    
  function createALink(href, text) {
    let a = document.createElement("a");
    a.href = href;
    a.text = text;
    a.target = "_blank";
    return a;
  }
  
  function br() { return document.createElement("br") }
  
  function createSpan(className, text) {
      let span = document.createElement("span");
      span.className = className;
      span.innerText = text;
      return span;
  }
  
  let liensPersoTable = document.createElement("table");
  liensPersoTable.id = "liensPerso";
  
  function addSection(text) {
    let row = document.createElement("tr");
    let cell = document.createElement("th");
    cell.innerText = text;
    row.appendChild(cell);
    liensPersoTable.appendChild(row);
  }
  
  function addSectionInfosPersos() {
    addSection("Infos perso");
    let row = liensPersoTable.insertRow();
    let cell = row.insertCell();
    cell.appendChild(createALink("evenement.php","Mes évènements"));
    cell.appendChild(br());
    cell.appendChild(createALink("sac.php","Mon inventaire"));
  }
  
  function addSectionCorrespondance() {
    addSection("Correspondance");
    let row = liensPersoTable.insertRow();
    let cell = row.insertCell();
    cell.appendChild(createALink("changer_message.php","Message du jour"));
    cell.appendChild(br());
    cell.appendChild(createALink("messagerie.php","Télégramme"));
    let spanMessages = document.querySelector("a[href='messagerie.php']").parentElement.querySelector("span");
    if(spanMessages != null) {
      cell.appendChild(createSpan("red"," ("+spanMessages.innerText+")"));
    }
  }
  
  function addSectionActionsSpeciales() {
    addSection("Actions spéciales");
    let row = liensPersoTable.insertRow();
    let cell = row.insertCell();
    cell.appendChild(createALink("ameliorer.php","Améliorer ce perso"));
    cell.appendChild(br());
    cell.appendChild(createALink("choix_rapatriement.php","Mes respawns"));
  }
  
  addSectionInfosPersos();
  addSectionCorrespondance();
  addSectionActionsSpeciales();
  
  injectLiensPersoTable();
  
  function injectLiensPersoTable() {
    let parentElement = document.getElementById("moveTableContainer").rows[0];
    let td = parentElement.insertCell(0);
    td.appendChild(liensPersoTable);
  }
  
});