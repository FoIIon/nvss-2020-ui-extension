chrome.storage.local.get("nostalgic", function(items) {
  if(!items.nostalgic) { return; }

  let visuTable = document.getElementById("visuTable");

  // Clone X coordinates
  visuTable.rows[0].cells[0].innerText = '';
  let xRows = visuTable.rows[0].cloneNode(true);
  visuTable.appendChild(xRows);

  // Clone Y Coordinates
  for(let row of visuTable.rows) {
    row.cells[0].removeAttribute("width");
    row.cells[0].style.paddingLeft="1px";
    row.cells[0].style.paddingRight="5px";
    let firstCell = row.cells[0].cloneNode(true);
    row.appendChild(firstCell);
  }
    
  let nbCols = visuTable.rows[0].cells.length;

  // HEADER
  let headerTr = document.createElement("tr");
  let headerTd = document.createElement("td");
  headerTd.setAttribute("colspan", nbCols);
  headerTd.innerHTML = '<a href="carte2.php" target="_blank">Ouvrir la carte</a>';
  headerTr.appendChild(headerTd);
  visuTable.insertBefore(headerTd, visuTable.firstChild);
  
  // FOOTER
  let footerTr = document.createElement("tr");
  let footerTd = document.createElement("td");
  footerTd.setAttribute("colspan", nbCols);
  footerTd.innerHTML = '<b>CLIQUEZ SUR LA VISU !</b>';
  footerTr.appendChild(footerTd);
  visuTable.appendChild(footerTr);

});