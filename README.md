# nvss 2020 ui extension

## Installation (Chrome)
- Télécharger le zip : https://gitlab.com/wil3347/nvss-2020-ui-extension/-/archive/master/nvss-2020-ui-extension-master.zip
- Dézipper dans un dossier
- Ouvrir la page des Extensions : chrome://extensions/
- Activer le mode développeur si ce n'est pas déjà activé
- Cliquer sur charger l'extension non empaquetée
- Sélectionner le dossier créé lors du dézippage

## Installation (Firefox)
- A installer directement via Firefox : https://addons.mozilla.org/fr/firefox/addon/nvss-2020-ui-extension/

## Pack d'images
Il est possible d'utiliser différents packs d'images personnalisées :
![nvss-ui-extension-v0-4](https://gitlab.com/wil3347/nvss-2020-ui-extension/-/wikis/uploads/71ad31ae3d490bc399d362aaa7dc1d7f/nvss-ui-extension-v0-4.gif)

(voir le paragraphe [Pack d'images préféré](#pack-dimages-préféré))

Si vous souhaitez intégrer un pack d'images dans l'extension, vous pouvez prendre contact avec wil.

## Recherche de destinataires
L'extension ajoute un champ de recherche de destinataire dans la page de composition d'un nouveau message.  

**Pré-requis : Aller sur la page de classement afin que l'extension récupère la liste des persos et leur matricule**

![recherche_destinataire_telegramme](/uploads/4064b3f947feb6f252ebdc855ee6ee37/recherche_destinataire_telegramme.gif)

## Editeur de message
L'extension ajoute un editeur dans la page de composition de nouveau message :
![image](/uploads/e44aaa861fae8c2893ac753a26451565/image.png)

### Exemple d'utilisation basique de l'édtieur de message
Edition d'un message (en mode texte brut) :  
![image](/uploads/4167c9feaa2eb05f8916baa4ecec38cc/image.png)

Edition du même message (en mode WYSIWYG / What You See Is What You Get) :  
![image](/uploads/1f2481e1026c75773f60c2796a8c836a/image.png)

Lecture du message reçu :  
![image](/uploads/117fc78cc5e58113df03709fb64f191b/image.png)

Insertion d'une image + Résultat :  
![image](/uploads/ed07e61a56bd13da52af7f2604c2a059/image.png)
![image](/uploads/8bc9b137728263f38f658b13a8590369/image.png)

## Carte Stratégique
L'extension remplace le titre de la page par les coordonnées au survol de la carte straégique  
![image](/uploads/744c526475bfe39df31939c00735409e/image.png)

## Options
### Chrome 
![image](/uploads/d036ce5eca86b6d8253ae99cb0d3c2a4/image.png)

![image](/uploads/de2f2148abbfec6920da4d085e6001b5/image.png)

### Firefox
![image](/uploads/4f5323eafba3f06de6a55d552b46cb4e/image.png)

![image](/uploads/2ebd23d599b8be00c5b493f80caa07de/image.png)

### Description des options
#### Je suis nostalgique
- Valeur par défaut : `actif`
- Désactivez cette option pour afficher le jeu dans son design réel
- L'éditeur de message n'est pas impacté par cette option
- Le pack d'images n'est pas impacté par cette option

#### Pack d'images préféré
- Valeur par défaut : `default`
- `default` est le pack officiel du jeu
- `old` est le pack officiel du jeu de 2003-2009
- `wil` est le pack utilisé par le joueur wil
